var app = require('express')();
var http = require('http').Server(app);
var mysql = require('mysql');
var uuid = require('node-uuid');
var bodyParser = require("body-parser");
var connection = mysql.createConnection({
	host	 : 'localhost',
	user	 : 'root',
	password : '',
	database : 'db_pos',
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/*module export*/
var katModel = require("./app/models/kategori.js");
var bagModel = require("./app/models/bagian.js");
var mejaketModel = require("./app/models/meja_ket.js");
var pajak = require("./app/models/pajak.js");

app.get('/', function (req ,res){
	res.send("hy");
});
/*kategori*/
app.get('/kategori', katModel.get)
app.get('/kategori/:id', katModel.getid)
app.post('/kategori', katModel.post)
app.put('/kategori', katModel.put)
app.delete('/kategori', katModel.del)

/*bagian*/
app.get('/bagian', bagModel.get)
app.get('/bagian/:id', bagModel.getid)
app.post('/bagian', bagModel.post)
app.put('/bagian', bagModel.put)
app.delete('/bagian', bagModel.del)

/*mejak_keterangan*/
app.get('/mejaket', mejaketModel.get)
app.get('/mejaket/:id', mejaketModel.getid)
app.post('/mejaket', mejaketModel.post)
app.put('/mejaket', mejaketModel.put)
app.delete('/mejaket', mejaketModel.del)

//pajak
app.get('/pajak',pajak.get);
app.get('/pajak/:id',pajak.get_id);


app.listen(3000);
 
 console.log("Berjalan di port 3000");
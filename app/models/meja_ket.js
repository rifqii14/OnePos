var mysql = require('mysql');
var uuid = require('node-uuid');
var conn = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'db_pos',
});

var data = {
    "error": "Terjadi Error",
    "status":""
};


module.exports = {
    get: function (req, res){

    conn.query("SELECT * from tbl_meja_keterangan", function (err, rows, fields){
        if(rows.length !=0){
            data["error"] = err;
            data["status"] = rows;
            res.json(data);
        }else{
            data["status"] = 'tidak ditemukan';
            res.json(data);
        }
        });
    },


    getid: function (req, res){
    var Id =req.params.id;
    conn.query("SELECT * from tbl_meja_keterangan WHERE id=?" ,[Id], function (err, rows, fields){
        if(rows.length !=0){
            data["error"] = err;
            data["status"] = rows;
            res.json(data);
        }else{
            data["status"] = 'tidak ditemukan';
            res.json(data);
        }
        });
    },

    post: function (req, res){
    var Id = uuid.v4();
    var Keterangan = req.body.keterangan;
        if(Id && Keterangan){
            conn.query("INSERT INTO tbl_meja_keterangan VALUES (?,?)",[Id,Keterangan],
            function (err, rows, fields){
                if(err){
                    data["error"] = err;
                    data["status"] = "Error menambahkan";
                }
                else{
                    data["error"] = "tidak ada error";
                    data["status"] = "Sukses menambahkan";
                }
                res.json(data);
            });
        }
        else{
            data["status"] = "Tolong masukkan datanya";
            res.json(data);
        }
    },

    put: function (req, res){
    var Id = req.body.id;
    var Keterangan = req.body.keterangan;
        if(Keterangan){
            conn.query("UPDATE tbl_meja_keterangan SET keterangan=? WHERE id=?", [Keterangan,Id],
            function (err, rows, fields){
                if(err){
                    data["error"] = err;
                    data["status"] = "Error mengupdate";
                }
                else{
                    data["error"] = "tidak ada error";
                    data["status"] = "Sukses mengupdate";
                }
                res.json(data);
            });
        }
        else{
            data["status"] = "Tolong masukkan datanya";
            res.json(data);
        }
    },

    del: function (req, res){
    var Id = req.body.id;
        if(Id){
            conn.query("DELETE FROM tbl_meja_keterangan WHERE id=?",[Id],
            function (err, rows, fields){
                if(err){
                    data["error"] = err;
                    data["status"] = "Error menghapus";
                }
                else{
                    data["error"] = "tidak ada error";
                    data["status"] = "Sukses menghapus";
                }
                res.json(data);
            });
        }
        else{
            data["status"] = "Tolong masukkan datanya";
            res.json(data);
        }
    }

}
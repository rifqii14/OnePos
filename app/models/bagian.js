var mysql = require('mysql');
var uuid = require('node-uuid');
var conn = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'db_pos',
});

var data = {
    "error": "Terjadi Error",
    "status":""
};

module.exports = {
    get: function (req, res){

    conn.query("SELECT * from tbl_bagian", function (err, rows, fields){
        if(rows.length !=0){
            data["error"] = err;
            data["status"] = rows;
            res.json(data);
        }else{
            data["status"] = 'tidak ditemukan';
            res.json(data);
        }
        });
    },


    getid: function (req, res){
    var Id =req.params.id;
    conn.query("SELECT * from tbl_bagian WHERE id=?" ,[Id], function (err, rows, fields){
        if(rows.length !=0){
            data["error"] = err;
            data["status"] = rows;
            res.json(data);
        }else{
            data["status"] = 'tidak ditemukan';
            res.json(data);
        }
        });
    },

    post: function (req, res){
    var Id = uuid.v4();
    var Bagian = req.body.bagian;
        if(Id && Bagian){
            conn.query("INSERT INTO tbl_bagian VALUES (?,?)",[Id,Bagian],
            function (err, rows, fields){
                if(err){
                    data["error"] = err;
                    data["status"] = "Error menambahkan";
                }
                else{
                    data["error"] = "tidak ada error";
                    data["status"] = "Sukses menambahkan";
                }
                res.json(data);
            });
        }
        else{
            data["status"] = "Tolong masukkan datanya";
            res.json(data);
        }
    },

    put: function (req, res){
    var Id = req.body.id;
    var Bagian = req.body.bagian;
        if(Bagian){
            conn.query("UPDATE tbl_bagian SET bagian=? WHERE id=?", [Bagian,Id],
            function (err, rows, fields){
                if(err){
                    data["error"] = err;
                    data["status"] = "Error mengupdate";
                }
                else{
                    data["error"] = "tidak ada error";
                    data["status"] = "Sukses mengupdate";
                }
                res.json(data);
            });
        }
        else{
            data["status"] = "Tolong masukkan datanya";
            res.json(data);
        }
    },

    del: function (req, res){
    var Id = req.body.id;
        if(Id){
            conn.query("DELETE FROM tbl_bagian WHERE id=?",[Id],
            function (err, rows, fields){
                if(err){
                    data["error"] = err;
                    data["status"] = "Error menghapus";
                }
                else{
                    data["error"] = "tidak ada error";
                    data["status"] = "Sukses menghapus";
                }
                res.json(data);
            });
        }
        else{
            data["status"] = "Tolong masukkan datanya";
            res.json(data);
        }
    }

}




